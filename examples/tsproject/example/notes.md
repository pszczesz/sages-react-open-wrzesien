# INstall TS
npm i -g typescript
tsc --version

# Compile and watch changes
tsc --watch index.ts
tsc -w index.ts

# Run compiled code
node index.js

# Ambient type declaration *.d.ts
tsc -d --allowJs lib.js 

# Project config tsconfig.json
tsc --strict --init
message TS6071: Successfully created a tsconfig.json file.
