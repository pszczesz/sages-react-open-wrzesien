import { Playlist } from "../../core/model/Playlist";
import { cls } from "../../core/utils/utils";

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
  onSelect: (id: Playlist["id"]) => void;
  onRemove: (id: Playlist["id"]) => void;
}

export const PlaylistsList = ({
  playlists,
  selectedId,
  onSelect,
  onRemove,
}: Props) => {
  return (
    <div>
      <div className="list-group">
        {playlists.map((playlist, index) => (
          <div
            data-playlist-id={playlist.id}
            className={cls(
              "list-group-item list-group-item-action",
              playlist.id === selectedId && "active"
            )}
            key={playlist.id}
            onClick={() => onSelect(playlist.id)}
          >
            {index + 1}. {playlist.name}
            <span
              className="close float-end"
              onClick={(e) => {
                onRemove(playlist.id);
                e.stopPropagation();
              }}
            >
              &times;
            </span>
          </div>
        ))}
      </div>
    </div>
  );
};
