import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { reducer as playlists } from './core/reducers/PlaylistsReducer'



const counter = (state = 0, action: any) => {
    switch (action.type) {
        case 'INC': return state + action.payload
        case 'DEC': return state - action.payload
        default: return state
    }
};


const reducer = combineReducers({
    counter: counter,
    playlists: playlists
})

export type AppState = ReturnType<typeof reducer>

// const logger: Middleware = (api: MiddlewareAPI) => (next: Dispatch) => (action: any) => {
//     console.log(action.type, action);
//     next(action)
// }

// const asyncThunk: Middleware = (api: MiddlewareAPI) => (next: Dispatch) => (action: any) => {
//     if (typeof action === 'function') {
//         action(api.dispatch, api.getState)
//     } else {
//         next(action)
//     }
// }

const middleware = [logger, thunk]

export const store = createStore(reducer, composeWithDevTools(
    applyMiddleware(...middleware)
));

(window as any).store = store;
