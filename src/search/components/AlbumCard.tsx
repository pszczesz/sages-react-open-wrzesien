import { Link, useHistory } from "react-router-dom";
import { SimpleAlbum } from "../../core/model/Search";

interface Props {
  album: SimpleAlbum;
}

export const AlbumCard = ({ album }: Props) => {
  const { push } = useHistory();

  const goToAlbum = () => {
    push(`/albums/${album.id}`);
  };

  return (
    // <Link to={"/albums/" + album.id}>
    <div className="card" onClick={goToAlbum}>
      <img src={album.images[0].url} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
        {/* <p className="card-text">
          This is a longer card with supporting text below as a natural lead-in
          to additional content. This content is a little bit longer.
        </p> */}
      </div>
    </div>
    // </Link>
  );
};
