
import "bootstrap/dist/css/bootstrap.css";
import { SearchView } from "./search/containers/SearchView";
import { NavBar } from "./core/components/NavBar";
import { Redirect, Route, Switch } from "react-router-dom";
import { AlbumDetailsView } from "./search/containers/AlbumDetailsView";
import { PlaylistsReducerView } from "./playlists/containers/PlaylistsReducerView";

function App() {
  return (
    <div className="App">
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col">
            <Switch>
              <Redirect path="/" exact to="/search" />
              <Route path="/search" exact component={SearchView} />
              <Route path="/playlists" component={PlaylistsReducerView} />
              <Route path="/albums/:album_id" component={AlbumDetailsView} />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
