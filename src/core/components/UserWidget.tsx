import React, { useContext } from "react";
import { UserProfileContext } from "../contexts/UserProfileContext";

interface Props {}

export const UserWidget = ({}: Props) => {
  const { user, login, logout } = useContext(UserProfileContext);

  return (
    <>
      {user ? (
        <div>
          Welcome {user.display_name} | <span onClick={logout}>Log Out</span>
        </div>
      ) : (
        <div>
          Welcome Guest | <span onClick={login}>Log In</span>
        </div>
      )}
    </>
  );
};
