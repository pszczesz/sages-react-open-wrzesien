import { playlistsMock } from "../model/fixtures";
import { Playlist } from "../model/Playlist";
import { PlaylistsState, reducer, playlistsLoadStart, playlistsLoadSuccess, fetchPlaylists } from "./PlaylistsReducer";

import { fetchPlaylistByUserId } from "../services/search";
import { PagingObject } from "../model/Search";
import { User } from "../model/User";

jest.mock("../services/search")

function mocked<F extends (...params: any) => any>(fn: F) {
  return fn as jest.MockedFunction<F>
}


describe('Playlists Reducer', () => {

  it('creates initial state', () => {
    const state: PlaylistsState = reducer(undefined, {} as any)
    expect(state).toEqual<PlaylistsState>({
      items: [],
      loading: false,
      selectedId: undefined,
      mode: 'details'
    })
  });

  it('loads playlits', () => {
    // Arrange - Given ...
    let state: PlaylistsState = reducer(undefined, {} as any)

    // Act - When ...
    state = reducer(state, playlistsLoadStart())

    // Assert - Then ...
    expect(state.loading).toBeTruthy()

    state = reducer(state, playlistsLoadSuccess(playlistsMock))
    expect(state.items).toEqual(playlistsMock)

    // expect(state).toEqual<PlaylistsState>(expect.objectContaining({
    //   loading: true,
    // }))
  })

  it('fetches playlists', (done) => {    // Arrange - Given ...
    // jest.useFakeTimers();

    const dispatchSpy = jest.fn()
    let state: PlaylistsState = reducer(undefined, {} as any)


    // type ReturnType<T extends (...params: any) => any> = T extends ((...params: any) => infer R) ? R : never;
    // type Parameters<T extends (...params: any) => any> = T extends ((...params: infer R) => any) ? R : never;
    type Fparams = Parameters<typeof fetchPlaylistByUserId>
    type Freturn = ReturnType<typeof fetchPlaylistByUserId>
    // const mock = (fetchPlaylistByUserId as unknown as jest.Mock<Freturn , Fparams>)
    // const mock = (fetchPlaylistByUserId as unknown as jest.Mock<Promise<PagingObject<Playlist>>, [User['id']]>)

    type MakeMock<T extends (...params: any) => any> = jest.Mock<ReturnType<T>, Parameters<T>>
    // const mock = (fetchPlaylistByUserId as unknown as MakeMock<typeof fetchPlaylistByUserId>)

    // const mock = (fetchPlaylistByUserId as jest.MockedFunction<typeof fetchPlaylistByUserId>)
    
    const mock = mocked(fetchPlaylistByUserId)

    mock.mockResolvedValue({ items: playlistsMock } as unknown as PagingObject<Playlist>)

    fetchPlaylists('123')(dispatchSpy)

    // jest.advanceTimersByTime(100)

    process.nextTick(() => {

      console.log(dispatchSpy.mock.calls)

      expect(mock).toHaveBeenCalledWith('123')
      expect(dispatchSpy).toHaveBeenCalledWith(playlistsLoadStart())
      expect(dispatchSpy).toHaveBeenCalledWith(playlistsLoadSuccess(playlistsMock))

      done()
    })
    // jest.runAllTicks()

  })


});